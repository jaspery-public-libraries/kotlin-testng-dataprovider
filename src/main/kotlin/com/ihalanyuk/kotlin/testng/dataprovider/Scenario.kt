/*
 * Copyright © 2018. Ihor Halanyuk <ihor.halaniuk@gmail.com>
 */

package com.ihalanyuk.kotlin.testng.dataprovider

class Scenario private constructor(val params: List<Any>) {
    private constructor(builder: Builder) : this(builder.params)

    class Builder(args: Array<out Any>) {
        internal val params = args.toList()
        fun build() = Scenario(this)
    }
}