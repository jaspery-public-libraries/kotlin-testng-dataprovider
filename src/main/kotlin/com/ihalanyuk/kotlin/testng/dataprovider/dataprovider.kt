/*
 * Copyright © 2018. Ihor Halanyuk <ihor.halaniuk@gmail.com>
 */

package com.ihalanyuk.kotlin.testng.dataprovider

/**
 * Introduces custom DSL for TestNG data providers. Example:
 * ```
 *  @DataProvider
 *  fun createRangeDataProvider(): Array<Array<Any>> {
 *      return dataProvider {
 *          scenario(LocalDate.parse("2017-01-01"), LocalDate.parse("2017-01-07"))
 *          scenario(LocalDate.parse("2017-04-05"), LocalDate.parse("2017-05-25"))
 *          scenario(LocalDate.parse("2017-05-25"), LocalDate.parse("2017-05-25"))
 *      }.testNGDataArray()
 *  }
 * ```
 *
 * Same example, more concise:
 * ```
 *  @DataProvider
 *  fun createRangeDataProvider() = dataProvider {
 *      scenario(LocalDate.parse("2017-01-01"), LocalDate.parse("2017-01-07"))
 *      scenario(LocalDate.parse("2017-04-05"), LocalDate.parse("2017-05-25"))
 *      scenario(LocalDate.parse("2017-05-25"), LocalDate.parse("2017-05-25"))
 *  }.testNGDataArray()
 *  ```
 */
fun dataProvider(block: Scenarios.Builder.() -> Unit) = Scenarios.Builder().apply(block).build()
