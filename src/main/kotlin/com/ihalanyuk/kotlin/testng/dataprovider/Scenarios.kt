/*
 * Copyright © 2018. Ihor Halanyuk <ihor.halaniuk@gmail.com>
 */

package com.ihalanyuk.kotlin.testng.dataprovider

class Scenarios private constructor(private val scenarios: List<Scenario>) {
    private constructor (builder: Builder) : this(builder.scenarios)

    fun testNGDataArray(): Array<Array<Any>> = scenarios.map { it.params.toTypedArray() }.toTypedArray()

    class Builder {
        internal val scenarios = mutableListOf<Scenario>()

        fun scenario(vararg args: Any) {
            scenarios.add(Scenario.Builder(args).build())
        }

        fun build() = Scenarios(this)
    }
}


