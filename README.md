# Kotlin-TestNG-DataProvider
> Kotlin DSL for creating TestNG data-provider.

Quick example:
```kotlin
 @DataProvider
 fun createRangeDataProvider() = dataProvider {
     scenario(10, "2017-01-07")
     scenario(15, "2017-05-25")
     scenario(20, "2017-05-25")
 }.testNGDataArray()
```

## Installing / Getting started

To use most recent snapshot version of library in your project,
add this to your gradle file:

```gradle
dependencies {
    implementation 'com.gitlab.jaspery-public-libraries:kotlin-testng-dataprovider:-SNAPSHOT'
}
```

There are no stable releases of this library yet.

## Features

Here is full example from library test folder:

```kotlin
@Suppress("RemoveRedundantBackticks")
class TestNGDataProviderTest {
    @DataProvider
    fun dataProviderTestNGDSL() = dataProvider {
        scenario(1, "I like to run", Pair("key", 2.5))
    }.testNGDataArray()

    @Test(dataProvider = "dataProviderTestNGDSL")
    fun `test data-provider DSL`(num: Int, str: String, any: Any) {
        assertEquals(num, 1)
        assertEquals(str, "I like to run")
        if (any is Pair<*, *>) {
            assertEquals(any.first, "key")
            assertEquals(any.second, 2.5)
        } else {
            fail("any should be instance of Pair, but is $any")
        }
    }
}
```
See [TestNGDataProviderTest](https://gitlab.com/jaspery-public-libraries/kotlin-testng-dataprovider/blob/master/src/test/kotlin/com/ihalanyuk/kotlin/testng/dataprovider/TestNGDataProviderTest.kt)

## Deployment / Publishing

Code is hosted on [GitLab](https://gitlab.com/jaspery-public-libraries/kotlin-testng-dataprovider)
and published via [JitPack](https://jitpack.io)

## Links

- Project homepage: https://gitlab.com/jaspery-public-libraries/kotlin-testng-dataprovider
- Repository: https://gitlab.com/jaspery-public-libraries/kotlin-testng-dataprovider
- Related projects:
  - TBD

## Licensing

Pre-release (snapshot) code in this project is licensed under GPL v3.0 license.
This may change in the future.

## About Author

Copyright © 2018. Ihor Halanyuk ihor.halaniuk (at) gmail.com
